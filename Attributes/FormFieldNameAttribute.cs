namespace Extensions.Attributes
{
    public class FormFieldNameAttribute : Attribute
    {
        public string Name { get; set; }
        public FormFieldNameAttribute(string name)
        {
            this.Name = name;
        }
    }
}