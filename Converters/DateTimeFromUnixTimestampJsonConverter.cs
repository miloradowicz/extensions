using System.Globalization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Extensions.Converters
{
    public sealed class DateTimeFromUnixTimestampJsonConverter : JsonConverter<DateTime>
    {
        public override DateTime Read(
            ref Utf8JsonReader reader, Type
            typeToConvert,
            JsonSerializerOptions options) => FromUnixTimestamp(reader.GetInt64());

        public override void Write(
            Utf8JsonWriter writer,
            DateTime value,
            JsonSerializerOptions options) => writer.WriteStringValue((value - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds.ToString(CultureInfo.InvariantCulture));

        private static DateTime FromUnixTimestamp(long unixTimestamp) =>
            new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(unixTimestamp).ToLocalTime();
    }
}