using System.Globalization;
using System.Reflection;
using System.Text;
using System.Web;
using Extensions.Attributes;
using Extensions.Interfaces;

namespace Extensions
{
    public static class Extensions
    {
        public static string AsFormUrlEncoded(this IFormUrlEncodable payload, string? root = null)
        {
            StringBuilder result = new StringBuilder();

            PropertyInfo[] props = payload.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo p in props)
            {
                Attribute[] attrs = Attribute.GetCustomAttributes(p, true);

                if (attrs.OfType<FormIgnoreAttribute>().FirstOrDefault() is not null)
                {
                    continue;
                }

                string fieldName = attrs.OfType<FormFieldNameAttribute>().FirstOrDefault()?.Name ?? p.Name;

                object? propValue = p.GetValue(payload);

                if (propValue is null || (propValue is IEnumerable<object> t && t.FirstOrDefault() is null))
                {
                    continue;
                }

                if (result.Length > 0)
                {
                    result.Append('&');
                }

                switch (propValue!)
                {
                    case IEnumerable<IFormUrlEncodable> pv:
                    {
                        result.AppendJoin('&', pv.Select((x, i) => x.AsFormUrlEncoded(root is null ? $"{fieldName}[{i}]" : $"{root}[{fieldName}][{i}]")));
                        break;
                    }

                    case IEnumerable<object> pv:
                    {
                        result.AppendJoin('&', pv.Select(x => string.Format("{0}[]={1}", root is null ? $"{fieldName}[]" : $"{root}[{fieldName}][]", HttpUtility.UrlEncode(
                            x switch
                            {
                                int @int => @int.ToString(CultureInfo.InvariantCulture),
                                long @long => @long.ToString(CultureInfo.InvariantCulture),
                                float @float => @float.ToString(CultureInfo.InvariantCulture),
                                double @double => @double.ToString(CultureInfo.InvariantCulture),
                                decimal @decimal => @decimal.ToString(CultureInfo.InvariantCulture),
                                string @string => @string,
                                _ => x.ToString(),
                            }))));
                        break;
                    }

                    case IFormUrlEncodable pv:
                    {
                        result.Append(pv.AsFormUrlEncoded(root is null ? fieldName : $"{root}[{fieldName}]"));
                        break;
                    }

                    case object:
                    {
                        result.Append(string.Format("{0}={1}", root is null ? fieldName : $"{root}[{fieldName}]", HttpUtility.UrlEncode(
                            propValue switch
                            {
                                int @int => @int.ToString(CultureInfo.InvariantCulture),
                                long @long => @long.ToString(CultureInfo.InvariantCulture),
                                float @float => @float.ToString(CultureInfo.InvariantCulture),
                                double @double => @double.ToString(CultureInfo.InvariantCulture),
                                decimal @decimal => @decimal.ToString(CultureInfo.InvariantCulture),
                                string @string => @string,
                                _ => propValue.ToString(),
                            }
                        )));

                        break;
                    }

                    default:
                        throw new InvalidDataException("Should not have reached here.");
                }
            }

            return result.ToString();
        }
    }
}